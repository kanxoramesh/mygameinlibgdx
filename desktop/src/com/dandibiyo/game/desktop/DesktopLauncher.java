package com.dandibiyo.game.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.dandibiyo.game.StartRobot;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width=StartRobot.WIDTH;
		config.height=StartRobot.HEIGHT;
		new LwjglApplication(new StartRobot(), config);
	}
}

package com.dandibiyo.game;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.dandibiyo.game.CheckingClas.JointOperation;

public class StartRobot extends Game {
    public static int V_HEIGHT=480;
	public static int V_WIDTH=800;
	public static final int WIDTH = 800;
	public static final int HEIGHT = 600;
	public  static  final short DEFAUL_BIT=1;
	public  static  final short ROBOT_BIT=2;
	public static final short BRICK_BIT =8 ;
	public static final short ENEMY_BIT =4 ;


	public  static  final short COIN_BIT=16;
	public  static  final short DESTROYED_BIT=32;
	// Angle of rotation for dead zone (no movement)
	public static final float ACCEL_ANGLE_DEAD_ZONE = 5.0f;
	// Max angle of rotation needed to gain max movement velocity
	public static final float ACCEL_MAX_ANGLE_MAX_MOVEMENT = 20.0f;
	public static final String title = "my Robot Game";
	public SpriteBatch batch;

	public  static final float PPM = 100;
	@Override
	public void create() {
		batch = new SpriteBatch();
		setScreen(new PlayScreen(this));

	//setScreen(new com.dandibiyo.game.CheckingClas.CheckScreen(this));
	//	setScreen(new JointOperation(this));

	}


	@Override
	public void render() {

		super.render();
	}

}

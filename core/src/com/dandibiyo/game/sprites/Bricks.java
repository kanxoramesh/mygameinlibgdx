package com.dandibiyo.game.sprites;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.dandibiyo.game.PlayScreen;
import com.dandibiyo.game.StartRobot;

/**
 * Created by Ramesh on 11/11/2017.
 */

public class Bricks extends InterActiveTileObject {
    public Bricks(PlayScreen screen, Rectangle rect) {
        super(screen, rect);
        fixture.setUserData(this);
        SetCategoryFilter(StartRobot.BRICK_BIT);

    }

    @Override
    public void onHeadHit() {
        Gdx.app.log("Bricks collision","");
      //  getCell().setTile(null);

    }


}

package com.dandibiyo.game.sprites;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.EdgeShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.dandibiyo.game.PlayScreen;
import com.dandibiyo.game.StartRobot;

/**
 * Created by Ramesh on 11/12/2017.
 */

public class Bombar extends Enemy {
    public Bombar(PlayScreen screen, float x, float y) {
        super(new Texture("bombaWarr.png"),screen, x, y);
        setBounds(x,y,20/StartRobot.PPM,20/StartRobot.PPM);
    }

    @Override
    protected void defineEnemy(float x, float y) {
        BodyDef bodydef = new BodyDef();
        bodydef.position.set(x , y );
        bodydef.type = BodyDef.BodyType.DynamicBody;
        bdy = world.createBody(bodydef);
        FixtureDef fix = new FixtureDef();
        //CircleShape shap =new CircleShape();
        PolygonShape shape = new PolygonShape();

        fix.density=200f;
        shape.setAsBox(10 / StartRobot.PPM, 10 / StartRobot.PPM);

        //  shap.setRadius(20/StartRobot.PPM);
        fix.shape = shape;
        fix.filter.categoryBits=StartRobot.ENEMY_BIT;
        fix.filter.maskBits=StartRobot.DEFAUL_BIT | StartRobot.COIN_BIT |StartRobot.BRICK_BIT |StartRobot.ROBOT_BIT;
        // fix.density=0.1f;

        bdy.createFixture(fix);
        bdy.setActive(true);

    }
    public  void update(float dt)
    {
        setPosition(bdy.getPosition().x-getWidth()/2,bdy.getPosition().y-getHeight()/2);

    }
}

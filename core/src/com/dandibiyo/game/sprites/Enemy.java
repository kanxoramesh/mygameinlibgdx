package com.dandibiyo.game.sprites;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.dandibiyo.game.PlayScreen;
import com.dandibiyo.game.StartRobot;

/**
 * Created by Ramesh on 11/11/2017.
 */

public abstract class Enemy extends Sprite {
    PlayScreen screen;
    World world;
    public Body bdy;
    public Enemy(TextureRegion region, PlayScreen screen, float x, float y) {
        super(region);
        this.world= screen.getTheWorld();
        this.screen= screen;
        setPosition(x,y);
        defineEnemy( x, y);

    }
    public Enemy(Texture texture, PlayScreen screen, float x, float y) {
        super(texture);
        this.world= screen.getTheWorld();
        this.screen= screen;
        setPosition(x,y);
        defineEnemy( x, y);

    }

    protected  abstract void defineEnemy(float x, float y);


    public abstract void update(float dt);
}

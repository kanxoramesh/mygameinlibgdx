package com.dandibiyo.game.sprites;

import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Filter;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.dandibiyo.game.PlayScreen;
import com.dandibiyo.game.StartRobot;

/**
 * Created by Ramesh on 11/11/2017.
 */

public abstract class InterActiveTileObject {
    protected World world;
    protected TiledMap tiledMap;
    protected Rectangle bounds;
    protected Body body;
    protected Fixture fixture;
    protected TiledMapTile tiledMapTile;
    PlayScreen screen;

    public InterActiveTileObject(PlayScreen screen, Rectangle rect) {

        this.screen= screen;
        this.world = screen.getTheWorld();
        this.tiledMap = screen.getTiledMap();
        this.bounds = rect;
        BodyDef bdef = new BodyDef();
        PolygonShape shape = new PolygonShape();
        FixtureDef fdef = new FixtureDef();

        bdef.type = BodyDef.BodyType.StaticBody;
        bdef.position.set((rect.getX() + rect.getWidth() / 2) / StartRobot.PPM, (rect.getY() + rect.getHeight() / 2) / StartRobot.PPM);
        body = world.createBody(bdef);
        shape.setAsBox(rect.getWidth() / 2 / StartRobot.PPM, rect.getHeight() / 2 / StartRobot.PPM);
        fdef.shape = shape;
        fixture=body.createFixture(fdef);
    }
    public abstract void onHeadHit();
    public  void SetCategoryFilter(short filterBits)
    {
        Filter filter= new Filter();
        filter.categoryBits=filterBits;
        fixture.setFilterData(filter);
    }
    public TiledMapTileLayer.Cell getCell()
    {
        TiledMapTileLayer tiledMapTileLayer= (TiledMapTileLayer)tiledMap.getLayers().get(2);
        return tiledMapTileLayer.getCell((int)(body.getPosition().x*StartRobot.PPM/32),(int)(body.getPosition().y*StartRobot.PPM/32));

    }
}

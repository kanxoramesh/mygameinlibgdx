package com.dandibiyo.game.sprites;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.physics.box2d.World;
import com.dandibiyo.game.PlayScreen;
import com.dandibiyo.game.StartRobot;

/**
 * Created by Ramesh on 11/11/2017.
 */

public class Coin extends InterActiveTileObject {

    public Coin(PlayScreen screen, Rectangle bounds) {
        super(screen, bounds);
        fixture.setUserData(this);
        SetCategoryFilter(StartRobot.COIN_BIT);

    }

    @Override
    public void onHeadHit() {
        Gdx.app.log("Cooin collision","");

    }
}

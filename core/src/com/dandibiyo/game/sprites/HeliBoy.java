package com.dandibiyo.game.sprites;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.dandibiyo.game.PlayScreen;
import com.dandibiyo.game.StartRobot;

/**
 * Created by Ramesh on 11/11/2017.
 */

public class HeliBoy extends Enemy {
    Animation heliAnim;
    float setTime;
    public HeliBoy(PlayScreen screen,float x, float y) {
        super(screen.getHeliboyAtlas().findRegion("heliboy"),screen, x, y);
        setBounds(x,y,32/StartRobot.PPM,32/StartRobot.PPM);


    }


    @Override
    protected void defineEnemy(float x, float y) {
        {

            Array<TextureRegion> frames = new Array<TextureRegion>();
            frames.add(new TextureRegion(getTexture(), 0, 0,96, 96));
            frames.add(new TextureRegion(getTexture(), 96, 0, 96, 96));
            frames.add(new TextureRegion(getTexture(),  192, 0, 96, 96));
            frames.add(new TextureRegion(getTexture(), 288, 0, 96, 96));
            heliAnim = new Animation(0.05f, frames);
           // frames.clear();

            setTime=0;
            BodyDef bodydef = new BodyDef();
            bodydef.position.set(x , y );
            bodydef.type = BodyDef.BodyType.DynamicBody;
            bdy = world.createBody(bodydef);
            FixtureDef fix = new FixtureDef();
            //CircleShape shap =new CircleShape();
            PolygonShape shape = new PolygonShape();

            fix.density=100f;
            shape.setAsBox(10 / StartRobot.PPM, 10 / StartRobot.PPM);

            fix.shape = shape;
            fix.filter.categoryBits=StartRobot.ENEMY_BIT;
            fix.filter.maskBits=StartRobot.DEFAUL_BIT | StartRobot.COIN_BIT |StartRobot.BRICK_BIT |StartRobot.ROBOT_BIT;

            bdy.createFixture(fix);
            bdy.setActive(true);
            setBounds(0, 0, 32 / StartRobot.PPM, 32 / StartRobot.PPM);

            bdy.setGravityScale(0);

        }

    }

    @Override
    public void update(float dt) {
        setTime+=dt;
        bdy.setActive(true);
        setPosition(bdy.getPosition().x - getWidth() / 2, bdy.getPosition().y - getHeight() / 2);
        setRegion(heliAnim.getKeyFrame(setTime, true));
    }
}
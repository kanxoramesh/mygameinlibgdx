package com.dandibiyo.game.scenes;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.EdgeShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.dandibiyo.game.PlayScreen;
import com.dandibiyo.game.StartRobot;

/**
 * Created by Ramesh on 11/10/2017.
 */

public class Robot extends Sprite {


    public enum STATE {STANDING, RUNNING, JUMPING, FALLING, SWORDING, DUCKING}

    public STATE currentState, previousState;
    public Animation robotRun, sword;
    private float stateTimer;
    public Boolean runningRight;
    public World world;
    public Body bdy;
    private TextureRegion standRobot, ducking, falling;
    public Boolean isMoveRight;

    public Boolean isMoveBack;
    public Boolean isJump;
    public Boolean isDuck;
    public Boolean isSword;
    Rectangle rectangle;

    public Robot(World world, PlayScreen playScreen) {
        super(playScreen.getAtlas().findRegion("character1"));
        isDuck=false;
        isJump=false;
        isMoveBack=false;
        isMoveRight=false;
        isSword=false;
        this.world = world;
        currentState = STATE.STANDING;
        previousState = STATE.STANDING;
        stateTimer = 0;
        runningRight = true;
        rectangle= new Rectangle(0,0,0,0);
        Array<TextureRegion> frames = new Array<TextureRegion>();
        frames.add(new TextureRegion(getTexture(), 0, 200, 120, 200));
        frames.add(new TextureRegion(getTexture(), 120, 200, 120, 200));
        frames.add(new TextureRegion(getTexture(), 240, 200, 120, 200));
        frames.add(new TextureRegion(getTexture(), 120, 0, 120, 200));
        robotRun = new Animation(0.1f, frames);
        frames.clear();
        frames.add(playScreen.getAtlas().findRegion("swordCharacter1"));
        frames.add(playScreen.getAtlas().findRegion("swordCharacter3"));
        frames.add(playScreen.getAtlas().findRegion("swordCharacter1"));
        frames.add(playScreen.getAtlas().findRegion("swordDown"));
        sword = new Animation(0.1f, frames);
        frames.clear();

        defineRobot();
        standRobot = new TextureRegion(getTexture(), 0, 200, 120, 200);
        ducking = new TextureRegion(getTexture(), 240, 0, 120, 200);
        falling = playScreen.getAtlas().findRegion("character5");
        setBounds(0, 0, 32 / StartRobot.PPM, 52 / StartRobot.PPM);
        setRegion(standRobot);
    }

    public void update(Float dt) {
        setPosition(bdy.getPosition().x - getWidth() / 2, bdy.getPosition().y - getHeight() / 2);
        setRegion(getFrame(dt));

    }



    private TextureRegion getFrame(Float dt) {
        currentState = getState();
        TextureRegion region;
        switch (currentState) {
            case RUNNING:
                region = robotRun.getKeyFrame(stateTimer, true);
                break;
            case SWORDING:
                region = sword.getKeyFrame(stateTimer, true);
                break;
            case DUCKING:
                region = ducking;
                break;
            case FALLING:
                region = falling;
                break;

            case JUMPING:
                region = falling;
                break;
            case STANDING:

            default:
                region = standRobot;
                break;
        }
        if ((bdy.getLinearVelocity().x < 0 || !runningRight) && !region.isFlipX()) {
            region.flip(true, false);
            runningRight = false;
        } else if ((bdy.getLinearVelocity().x > 0 || runningRight) && region.isFlipX()) {
            region.flip(true, false);
            runningRight = true;
        }
        stateTimer = currentState == previousState ? stateTimer + dt : 0;
        previousState = currentState;
        return region;
    }

    private STATE getState() {
        if (getSword())
            return STATE.SWORDING;
            if (getDuck())
            return STATE.DUCKING;
        else if (bdy.getLinearVelocity().y > 0 || (bdy.getLinearVelocity().y < 0 && previousState == STATE.JUMPING))
            return STATE.JUMPING;
        else if (bdy.getLinearVelocity().y < 0)
            return STATE.FALLING;
        else if (bdy.getLinearVelocity().x != 0)
            return STATE.RUNNING;
        else
            return STATE.STANDING;

    }

    private void defineRobot() {
        BodyDef bodydef = new BodyDef();
        bodydef.position.set(100 / StartRobot.PPM, 160 / StartRobot.PPM);
        bodydef.type = BodyDef.BodyType.DynamicBody;
        bdy = world.createBody(bodydef);
        FixtureDef fix = new FixtureDef();
        //CircleShape shap =new CircleShape();
        PolygonShape shape = new PolygonShape();

        shape.setAsBox(10 / StartRobot.PPM, 25 / StartRobot.PPM);

        //  shap.setRadius(20/StartRobot.PPM);
        fix.shape = shape;
        fix.filter.categoryBits=StartRobot.ROBOT_BIT;
        fix.filter.maskBits=StartRobot.DEFAUL_BIT | StartRobot.COIN_BIT |StartRobot.BRICK_BIT |StartRobot.ENEMY_BIT;
       // fix.density=0.1f;

        bdy.createFixture(fix);

        EdgeShape head = new EdgeShape();
        head.set(new Vector2(-15 / StartRobot.PPM, 27 / StartRobot.PPM), new Vector2(15 / StartRobot.PPM, 27 / StartRobot.PPM));
        fix.shape = head;
        fix.isSensor = true;

        bdy.createFixture(fix).setUserData("head");

    }

    public Boolean getMoveRight() {
        return isMoveRight;
    }

    public void setMoveRight(Boolean moveRight) {
        isMoveRight = moveRight;
        if(isMoveRight)
        {
            bdy.applyLinearImpulse(new Vector2(0.7f, 0), bdy.getWorldCenter(), true);

        }
    }

    public Boolean getMoveBack() {
        return isMoveBack;
    }

    public void setMoveBack(Boolean moveBack) {
        isMoveBack = moveBack;
        if(isMoveBack)
        {
           bdy.applyLinearImpulse(new Vector2(-0.7f, 0),bdy.getWorldCenter(), true);

        }
    }

    public Boolean getJump() {
        return isJump;
    }

    public void setJump(Boolean jump) {
        isJump = jump;
        if(isJump)
          bdy.applyLinearImpulse(new Vector2(0, 3f), bdy.getWorldCenter(), true);

    }

    public Boolean getDuck() {
        return isDuck;
    }

    public void setDuck(Boolean duck) {
        isDuck = duck;
    }
    public  Boolean getSword()
    {
        return isSword;
    }
    public void setSword(boolean sword) {
        this.isSword = sword;
    }

}

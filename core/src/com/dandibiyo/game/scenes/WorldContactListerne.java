package com.dandibiyo.game.scenes;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.dandibiyo.game.sprites.InterActiveTileObject;

/**
 * Created by Ramesh on 11/11/2017.
 */

public class WorldContactListerne implements ContactListener {
    @Override
    public void beginContact(Contact contact) {

        Fixture fixA= contact.getFixtureA();
        Fixture fixB= contact.getFixtureB();
        if((fixA.getUserData()=="head") || fixB.getUserData()=="head")
        {
            Fixture head=fixA.getUserData()=="head"?fixA:fixB;
            Fixture object=fixA==head?fixB:fixA;
            if(object.getUserData()!=null && InterActiveTileObject.class.isAssignableFrom(object.getUserData().getClass()))
            {
                ((InterActiveTileObject)object.getUserData()).onHeadHit();
            }
        }
    }

    @Override
    public void endContact(Contact contact) {
        Gdx.app.log("end: ","end  contact");

    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {

    }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {

    }
}

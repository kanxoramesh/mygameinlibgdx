package com.dandibiyo.game.Tools;

import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.objects.CircleMapObject;
import com.badlogic.gdx.maps.objects.EllipseMapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Ellipse;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.dandibiyo.game.PlayScreen;
import com.dandibiyo.game.StartRobot;
import com.dandibiyo.game.sprites.Bombar;
import com.dandibiyo.game.sprites.Bricks;
import com.dandibiyo.game.sprites.Coin;
import com.dandibiyo.game.sprites.Enemy;
import com.dandibiyo.game.sprites.HeliBoy;
import com.dandibiyo.game.sprites.Turtle;

/**
 * Created by Ramesh on 11/11/2017.
 */

public class B2World {

    PlayScreen playScreen;
    World world;
    Array<Enemy> Bombararray;
    TiledMap tiledMap;
    public B2World(PlayScreen playScreen) {
        this.playScreen= playScreen;
        world= playScreen.getTheWorld();
        tiledMap=playScreen.getTiledMap();
        BodyDef bdef = new BodyDef();
        PolygonShape shape = new PolygonShape();
        FixtureDef fdef = new FixtureDef();
        Body body;

        for (MapObject obj : tiledMap.getLayers().get(2).getObjects().getByType(RectangleMapObject.class)) {
            Rectangle rect = ((RectangleMapObject) obj).getRectangle();
            new Bricks(playScreen,rect);

        }
        Bombararray= new Array<Enemy>();
        for (MapObject obj : tiledMap.getLayers().get(3).getObjects().getByType(RectangleMapObject.class)) {
            Rectangle rect = ((RectangleMapObject) obj).getRectangle();

           try {
               Bombararray.add(new Bombar(playScreen, rect.getX() / StartRobot.PPM, rect.getY() / StartRobot.PPM));
           }catch (Exception e)
           {
               e.printStackTrace();
           }
        }
        for (MapObject obj : tiledMap.getLayers().get(5).getObjects().getByType(RectangleMapObject.class)) {
            Rectangle rect = ((RectangleMapObject) obj).getRectangle();
            Bombararray.add(new HeliBoy(playScreen,rect.getX()/StartRobot.PPM,rect.getY()/StartRobot.PPM));
        }
        for (MapObject obj : tiledMap.getLayers().get(6).getObjects().getByType(RectangleMapObject.class)) {
            Rectangle rect = ((RectangleMapObject) obj).getRectangle();
            new Turtle(playScreen,rect);
        }

        for (MapObject obj : tiledMap.getLayers().get(4).getObjects().getByType(EllipseMapObject.class)) {
          /*  Ellipse ellipse = ((EllipseMapObject) obj).getEllipse();
            bdef.type = BodyDef.BodyType.StaticBody;
            bdef.position.set(ellipse.x/ StartRobot.PPM,ellipse.y/ StartRobot.PPM);
            body = world.createBody(bdef);
            shape.setRadius(4/StartRobot.PPM);
            fdef.shape = shape;
            body.createFixture(fdef);*/
        }

    }
    public Array<Enemy> getArray()
    {
        return Bombararray;
    }
}

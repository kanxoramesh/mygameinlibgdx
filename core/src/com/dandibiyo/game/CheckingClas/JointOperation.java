package com.dandibiyo.game.CheckingClas;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.joints.DistanceJointDef;
import com.badlogic.gdx.physics.box2d.joints.WeldJointDef;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.dandibiyo.game.StartRobot;

/**
 * Created by Ramesh on 11/12/2017.
 */

public class JointOperation implements Screen {

    SpriteBatch sb;
    StartRobot startRobot;
    public World world;
    public Body body;
    private BodyDef bdef;
    OrthographicCamera camera;
    Viewport viewport;
    public Box2DDebugRenderer renderer;
    public FixtureDef fixturedef;
    WeldJointDef weldJointDef;
    public JointOperation(StartRobot startRobot)
    {
        this.startRobot= startRobot;
        sb=startRobot.batch;
        world= new World(new Vector2(0,-10),true);
        camera= new OrthographicCamera();
        viewport= new StretchViewport(StartRobot.V_WIDTH,StartRobot.V_HEIGHT,camera);

        renderer= new Box2DDebugRenderer();
        bdef=new BodyDef();
        bdef.position.set(180,80);
        bdef.type= BodyDef.BodyType.DynamicBody;
        body= world.createBody(bdef);
        fixturedef= new FixtureDef();
        PolygonShape shape= new PolygonShape();
        shape.setAsBox(10,10);
        fixturedef.shape=shape;
        body.createFixture(fixturedef);
      /* weldJointDef = new WeldJointDef();
        weldJointDef.bodyA=body;
        weldJointDef.bodyB=body;
        weldJointDef.localAnchorA.set(0,0);
        weldJointDef.localAnchorB.set(.55f,0);
        world.createJoint(weldJointDef);
*/
     /*   DistanceJointDef distanceJointDef = new DistanceJointDef();
        distanceJointDef.bodyA=body;
        distanceJointDef.bodyB=body;
        distanceJointDef.length = 2.0f;
        distanceJointDef.localAnchorA.set(0,0);
        distanceJointDef.localAnchorB.set(0,0);
        world.createJoint(distanceJointDef);*/


    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        world.step(1 / 60f, 6, 2);

       body.applyLinearImpulse(new Vector2(0f,-body.getPosition().y*delta),body.getWorldCenter(),true);
        Gdx.gl.glClearColor(1,0,0,1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        sb.setProjectionMatrix(camera.combined);
        renderer.render(world,camera.combined);



    }

    @Override
    public void resize(int width, int height) {

        viewport.update(width,height);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}

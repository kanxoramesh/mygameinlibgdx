package com.dandibiyo.game.CheckingClas;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.EdgeShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.dandibiyo.game.StartRobot;

/**
 * Created by Ramesh on 11/12/2017.
 */

public class CheckScreen implements Screen {

    StartRobot s;
    public OrthographicCamera camera;
    Viewport viewport;
    private World world;
    private Box2DDebugRenderer b2dr;
    public Body b, b2;
    FixtureDef fixDef, fixDef2;
    BodyDef bodyDef, bodyDef2;


    SpriteBatch sb;
    Texture text;


    public CheckScreen(StartRobot startRobot) {
        this.s = startRobot;
        camera = new OrthographicCamera();
        text = new Texture("coinNew.png");
        viewport = new StretchViewport(s.V_WIDTH, s.V_HEIGHT, camera);
        world = new World(new Vector2(0, -10), true);
        bodyDef = new BodyDef();
        fixDef = new FixtureDef();
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        bodyDef.position.set(150, 80);
        b = world.createBody(bodyDef);
        PolygonShape shape = new PolygonShape();
        shape.setAsBox(5, 5);
        fixDef.shape = shape;
        fixDef.density=300;
        b.createFixture(fixDef);

        b.applyTorque(500f,true);
        bodyDef2 = new BodyDef();
        bodyDef2.type = BodyDef.BodyType.StaticBody;
        bodyDef2.position.set(0, 0);
        b2dr = new Box2DDebugRenderer();
        fixDef2 = new FixtureDef();
        b2 = world.createBody(bodyDef2);
        EdgeShape eShape = new EdgeShape();
        eShape.set(new Vector2(0, 0), new Vector2(200, 50));
        fixDef2.shape = eShape;
        fixDef2.friction=1.0f;
        fixDef2.restitution=1;
        b2.createFixture(fixDef2);


        sb = startRobot.batch;


    }

    @Override
    public void show() {

    }

    public void update(Float delta) {
    //  b.applyLinearImpulse(new Vector2(0, b.getPosition().y * delta), b.getWorldPoint(new Vector2(5,0)), true);
        b.applyAngularImpulse(60f/delta,true);
       // b.applyForce(new Vector2(0,-10),b.getWorldCenter(),true);
        b.setGravityScale(0);
    }

    @Override
    public void render(float delta) {


        world.step(1 / 60f, 6, 2);

      // update(delta);
        Gdx.gl.glClearColor(1, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        s.batch.setProjectionMatrix(camera.combined);
        b2dr.render(world, camera.combined);
        sb.begin();
        sb.draw(text, b.getPosition().x, b.getPosition().y,20,20);
        sb.end();

    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}

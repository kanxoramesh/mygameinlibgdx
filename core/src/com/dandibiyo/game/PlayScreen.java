package com.dandibiyo.game;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.dandibiyo.game.Tools.B2World;
import com.dandibiyo.game.scenes.Robot;
import com.dandibiyo.game.scenes.WorldContactListerne;
import com.badlogic.gdx.Input.Peripheral;
import com.dandibiyo.game.sprites.Bombar;
import com.dandibiyo.game.sprites.Enemy;

/**
 * Created by Ramesh on 11/10/2017.
 */

public class PlayScreen implements Screen, InputProcessor {

    StartRobot startRobot;
    public OrthographicCamera camera;
    Viewport viewport;
    //  Texture img;

    Array<Enemy> array;
    private World world;
    private Box2DDebugRenderer b2dr;
    private Bombar bombar;
    public B2World bCreator;


    TextureAtlas textureAtlas,heliboyAtlas;
    private TmxMapLoader mapLoader;
    private TiledMap tiledMap;
    private OrthogonalTiledMapRenderer renderer;
    Robot player;
    private boolean accelerometerAvailable;

    public PlayScreen(StartRobot startRobot) {
        this.startRobot = startRobot;
        textureAtlas = new TextureAtlas("robot/robotPackfile.pack");
        heliboyAtlas = new TextureAtlas("heliboypack.pack");
        camera = new OrthographicCamera();
        viewport = new StretchViewport((startRobot.V_WIDTH / StartRobot.PPM), startRobot.V_HEIGHT / StartRobot.PPM, camera);
        mapLoader = new TmxMapLoader();
        tiledMap = mapLoader.load("backgroundMap.tmx");
        renderer = new OrthogonalTiledMapRenderer(tiledMap, 1 / StartRobot.PPM);
        world = new World(new Vector2(0, -10), true);
        b2dr = new Box2DDebugRenderer();
       bCreator= new B2World(this);


        Gdx.app.log("max Size : ",""+Gdx.gl20.GL_MAX_TEXTURE_SIZE);
        System.out.println("max Size : "+Gdx.gl20.GL_MAX_TEXTURE_SIZE);
        player = new Robot(world, this);
        world.setContactListener(new WorldContactListerne());
        Gdx.input.setInputProcessor(this);
        accelerometerAvailable = Gdx.input.isPeripheralAvailable(
                Peripheral.Accelerometer);
        Array<Body>  bod= new Array<Body>();
        world.getBodies(bod);

        array= bCreator.getArray();

    }

    public World getTheWorld()
    {
        return world;
    }
    public TiledMap getTiledMap()
    {
        return tiledMap;
    }
    public TextureAtlas getAtlas() {
        return textureAtlas;
    }
    public TextureAtlas getHeliboyAtlas() {
        return heliboyAtlas;
    }

    @Override
    public void show() {
        camera.position.set((viewport.getWorldWidth() / 2), viewport.getWorldHeight() / 2, 0);

    }

    public void HandleInput(float dt) {

        if (accelerometerAvailable) {
            // normalize accelerometer values from [-10, 10] to [-1, 1]
            // which translate to rotations of [-90, 90] degrees
            float amount = Gdx.input.getAccelerometerY() / 10.0f;
            amount *= 90.0f;
            // is angle of rotation inside dead zone?
            if (Math.abs(amount) < StartRobot.ACCEL_ANGLE_DEAD_ZONE) {
                amount = 0;
            } else {
            // use the defined max angle of rotation instead of
            // the full 90 degrees for maximum velocity
                amount /= StartRobot.ACCEL_MAX_ANGLE_MAX_MOVEMENT;
            }
            player.bdy.applyLinearImpulse(new Vector2( amount/50, 0), player.bdy.getWorldCenter(), true);
        }
                // Execute auto-forward movement on non-desktop platform
        else if (Gdx.app.getType() != Application.ApplicationType.Desktop) {
            /*  if (Gdx.input.isKeyJustPressed(Input.Keys.UP)) {
            player.setDuck(false);

            player.bdy.applyLinearImpulse(new Vector2(0, 3f), player.bdy.getWorldCenter(), true);

        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.DOWN)) {
            player.setDuck(true);
        }
        if (Gdx.input.isKeyPressed(Input.Keys.RIGHT) && (player.bdy.getLinearVelocity().x <= 2)) {
            player.setDuck(false);

            player.bdy.applyLinearImpulse(new Vector2(0.1f, 0), player.bdy.getWorldCenter(), true);

        }
        if (Gdx.input.isKeyPressed(Input.Keys.LEFT) && (player.bdy.getLinearVelocity().x >= -2)) {
            player.setDuck(false);

            player.bdy.applyLinearImpulse(new Vector2(-0.1f, 0), player.bdy.getWorldCenter(), true);

        }
        if (Gdx.input.justTouched()) {
            player.setDuck(false);

            player.bdy.applyLinearImpulse(new Vector2(0, 3f), player.bdy.getWorldCenter(), true);
        }
*/
        }
    }

    public void update(float dt) {
          HandleInput(dt);
        world.step(1 / 60f, 6, 2);
        player.update(dt);
        System.out.println("player x position: "+player.bdy.getPosition().x);
        System.out.println("camera x position: "+camera.position.x);
        camera.position.x = player.bdy.getPosition().x + 3;
        if((camera.position.x-viewport.getWorldWidth()/2)<0)
            camera.position.x=viewport.getWorldWidth()/2;

            if(player.bdy.getPosition().x<0)
            {
                player.setPosition(0,player.bdy.getPosition().y);
            }



        renderer.setView(camera);
        camera.update();

        for(Enemy b:array)
        {
            b.update(dt);
            /*if(player.getX()>b.getX()-2)
                b.bdy.setActive(true);
            else b.bdy.setActive(false);
        */}
    }

    @Override
    public void render(float delta) {
        update(delta);
        Gdx.gl.glClearColor(1, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        renderer.render();

       // b2dr.render(world, camera.combined);
        startRobot.batch.setProjectionMatrix(camera.combined);
        startRobot.batch.begin();
        player.draw(startRobot.batch);
        for(Enemy b:array)
        {
            b.draw(startRobot.batch);
        }
        startRobot.batch.end();
    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

        tiledMap.dispose();
        b2dr.dispose();
        world.dispose();
        renderer.dispose();

    }

    @Override
    public boolean keyDown(int keycode) {

        switch (keycode) {
            case Input.Keys.RIGHT: {
                player.setMoveRight(true);

            }
            break;

            case Input.Keys.LEFT: {
                player.setMoveBack(true);


            }
            break;
            case Input.Keys.UP: {
                player.setJump(true);

            }
            break;
            case Input.Keys.CONTROL_RIGHT: {
                player.setSword(true);

            }
            break;

            case Input.Keys.DOWN:
                player.setDuck(true);
                break;

        }
        return true;
    }

    @Override
    public boolean keyUp(int keycode) {
        switch (keycode) {
            case Input.Keys.RIGHT: {
                player.setMoveRight(false);

            }
            break;
            case Input.Keys.LEFT: {
                player.setMoveBack(false);

            }
            break;
            case Input.Keys.UP: {
                player.setJump(true);
            }
            break;
            case Input.Keys.DOWN:
                player.setDuck(false);
                break;
            case Input.Keys.CONTROL_RIGHT: {
                player.setSword(false);

            }
            break;
        }
        return true;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        player.setJump(true);
        return true;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {

        player.setJump(false);
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
